package kz.greetgo.huawei.esf.dao;

import org.apache.ibatis.annotations.Select;

public interface TestDao {

  @Select("select 'Test Register: Hello World from MyBatis!'")
  String getHelloWordStr();

}
