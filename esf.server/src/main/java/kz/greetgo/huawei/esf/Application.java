package kz.greetgo.huawei.esf;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import kz.greetgo.huawei.esf.conf.AppFolderPath;
import kz.greetgo.huawei.esf.conf.beans.all.db.LiquibaseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

  @Autowired
  private LiquibaseManager liquibaseManager;

  public static void main(String[] args) {
    System.setProperty("spring.profiles.active", "dev");
    SpringApplication.run(Application.class, args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(Application.class);
  }

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    servletContext.setInitParameter("spring.profiles.active", "prod");
    super.onStartup(servletContext);
  }

  @PostConstruct
  public void init() {
    if (!AppFolderPath.do_not_run_liquibase().toFile().exists()) {
      liquibaseManager.apply();
    }
  }

}
