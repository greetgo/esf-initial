package kz.greetgo.huawei.esf.controller;

import io.swagger.annotations.Api;
import kz.greetgo.huawei.esf.register.TestRegister;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "/test", description = "Dev test controller")
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {

  @Autowired
  private final TestRegister register;
  
  @GetMapping(value = "check", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String check() {
    return "Hello World!";
  }
  
  @GetMapping(value = "register/check", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public String checkRegister() {
    return register.check();
  }

}
