package kz.greetgo.huawei.esf.conf.beans.all;


import kz.greetgo.conf.hot.FileConfigFactory;
import kz.greetgo.huawei.esf.conf.AppFolderPath;
import kz.greetgo.huawei.esf.hotconfig.DbConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AllConfigFactory extends FileConfigFactory {

  @Override
  protected String getBaseDir() {
    return AppFolderPath.confDif();
  }

  @Bean
  public DbConfig createPostgresDbConfig() {
    return createConfig(DbConfig.class);
  }

}
