package kz.greetgo.huawei.esf.register.impl;

import kz.greetgo.huawei.esf.dao.TestDao;
import kz.greetgo.huawei.esf.register.TestRegister;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestRegisterImpl implements TestRegister {

  private final TestDao testDao;

  @Override
  public String check() {
    return testDao.getHelloWordStr();
  }

}
