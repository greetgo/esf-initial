package kz.greetgo.huawei.esf.util;

import kz.greetgo.huawei.esf.Application;

public class ApplicationInfo {

  public static String appVersion() {
    return Application.class.getPackage().getImplementationVersion();
  }

}
