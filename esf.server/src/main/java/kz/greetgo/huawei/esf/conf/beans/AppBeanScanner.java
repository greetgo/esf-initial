package kz.greetgo.huawei.esf.conf.beans;

import kz.greetgo.huawei.esf.conf.beans.all.AllConfigFactory;
import kz.greetgo.huawei.esf.conf.beans.all.MyBatisConfiguration;
import kz.greetgo.huawei.esf.conf.beans.all.db.DataSourceConfiguration;
import kz.greetgo.huawei.esf.dao.BeanScannerDao;
import kz.greetgo.huawei.esf.hotconfig.BeanScannerHotConfig;
import kz.greetgo.huawei.esf.register.impl.BeanScannerRegister;
import org.springframework.context.annotation.Import;

@Import({
    BeanScannerDao.class,
    AllConfigFactory.class,
    BeanScannerRegister.class,
    MyBatisConfiguration.class,
    BeanScannerHotConfig.class,
    DataSourceConfiguration.class
})
public class AppBeanScanner {
}
