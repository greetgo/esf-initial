package kz.greetgo.huawei.esf.conf.beans.all;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("kz.greetgo.huawei.esf.dao")
public class MyBatisConfiguration {
}
