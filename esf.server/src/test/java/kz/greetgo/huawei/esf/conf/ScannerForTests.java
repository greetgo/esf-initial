package kz.greetgo.huawei.esf.conf;

import kz.greetgo.huawei.esf.conf.beans.AppBeanScanner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@Import({
    AppBeanScanner.class,
})
@ComponentScan(basePackages = "kz.greetgo.huawei.esf.register")
public class ScannerForTests {}
