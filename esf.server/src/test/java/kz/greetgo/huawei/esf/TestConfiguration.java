package kz.greetgo.huawei.esf;//package kz.greetgo.aix_service_bus.register;

import kz.greetgo.huawei.esf.conf.beans.AppBeanScanner;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@MapperScan(basePackages = {"kz.greetgo.huawei.esf.dao"})
@ComponentScan(basePackages = {"kz.greetgo.huawei.esf.dao", "kz.greetgo.huawei.esf.util"})
@Import({AppBeanScanner.class, JdbcTemplateAutoConfiguration.class})
public class TestConfiguration {

}

