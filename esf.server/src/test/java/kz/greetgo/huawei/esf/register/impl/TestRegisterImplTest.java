package kz.greetgo.huawei.esf.register.impl;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import kz.greetgo.huawei.esf.TestConfiguration;
import kz.greetgo.huawei.esf.register.TestRegister;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfiguration.class)
public class TestRegisterImplTest {


  @Autowired
  private TestRegister register;

  @Test
  public void test() {
    //
    //
    String check = register.check();
    //
    //

    assertThat(check).isNotNull();
  }

}