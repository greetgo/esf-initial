package kz.greetgo.huawei.esf._preparation_;


import kz.greetgo.huawei.esf._preparation_.beans.DbWorker;
import kz.greetgo.huawei.esf.conf.beans.all.db.liquibase.PostgresLiquibase;

public class RecreateDb {

  public static void main(String[] args) throws Exception {
    new DbWorker().recreate();
    new PostgresLiquibase().apply(false);
  }

}
